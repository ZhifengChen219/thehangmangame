package components;

import apptemplate.AppTemplate;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

/**
 * This interface provides the structure for file components in
 * our applications. Note that by doing so we make it possible
 * for customly provided descendent classes to have their methods
 * called from this framework.
 *
 * @author Richard McKenna, Ritwik Banerjee
 */
/**
 * @author Zhifeng Chen
 */
@SuppressWarnings("unused")
public interface AppFileComponent {

    void saveData(AppDataComponent data, Path filePath) throws IOException;

    AppDataComponent loadData(Path filePath, AppTemplate appTemplate) throws IOException;

    void exportData(AppDataComponent data, Path filePath) throws IOException;
}
