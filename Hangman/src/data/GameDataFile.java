package data;

import apptemplate.AppTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

/**
 * @author Ritwik Banerjee
 */

/**
 * @author Zhifeng Chen
 */
public class GameDataFile implements AppFileComponent {

    public static final String TARGET_WORD  = "TARGET_WORD";
    public static final String GOOD_GUESSES = "GOOD_GUESSES";
    public static final String BAD_GUESSES  = "BAD_GUESSES";
    public static final String REMAINING_GUESSES  = "REMAINING_GUESSES";

    @Override
    public void saveData(AppDataComponent data, Path to) throws IOException {
        GameData gameData = (GameData) data;
        ObjectMapper mapper = new ObjectMapper();
        Map<String,Object> gd = new HashMap<String,Object>();
        Set<Character> goodGuesses = (HashSet) gameData.getGoodGuesses();
        Set<Character> badGuesses = (HashSet) gameData.getBadGuesses();
        gd.put(TARGET_WORD, gameData.getTargetWord());
        gd.put(GOOD_GUESSES, goodGuesses);
        gd.put(BAD_GUESSES, badGuesses);
        gd.put(REMAINING_GUESSES, gameData.getRemainingGuesses());
        mapper.writeValue(to.toFile(), gd);

    }

    @Override
    public AppDataComponent loadData(Path from, AppTemplate appTemplate) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> gd = mapper.readValue(from.toFile(), Map.class);
        String targetWord = (String) gd.get(TARGET_WORD);
        List<Character> gg = (ArrayList) gd.get(GOOD_GUESSES);
        List<Character> bg = (ArrayList) gd.get(BAD_GUESSES);
        Set<Character> goodGuesses = new HashSet<Character>(gg);
        Set<Character> badGuesses = new HashSet<Character>(bg);
        int remainingGuesses = (int)gd.get(REMAINING_GUESSES);
        AppDataComponent gamedata = new GameData(targetWord, goodGuesses, badGuesses, remainingGuesses, appTemplate);
        return gamedata;
    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}
